var request = require("request");
var fs = require("fs")
var thecatapi = require('thecatapi-helper')
var param = {api_key:"MTIzODE2", type:"jpg", size:"full"};



/// Send cat to user
/// bot: the telegram bot api object
/// msg object
/// category: leave null for random cat
exports.sendCat = function(bot,msg, category){
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var link = thecatapi.linkbuilder(category, param.api_key, param.type, param.size)
  console.log(link)
  request({
        url : link,
        //make the returned body a Buffer
        encoding : null
    }, function(error, response, body) {
        if (error) {
            console.log(error)
            return
        }
        bot.sendPhoto(chatId, body)
    });
}


exports.helpMessage = ` This is MeowPicBot
/help Show help
/about More infomation about this bot
/meow Show random cat
/hats Cat in the hat
/sunglasses Cat with sunglasses
/boxes Cat in the boxes
/caturday Caturday
/ties Cat where ties
/sinks Cat in the sinks
/clothes Cat wear clothes
`;
