'use strict';

var TelegramBot = require('node-telegram-bot-api');
var TheCatAPI = require('./thecatapi');
var http = require('http');
var botanAPI_release = "xxFf28X4nOdkfO5QYKrWBAuVKBnw9s96";
var botanAPI_dev =     "Dx7wrShOa:8HzGJrhAG_J4OEL5fynlsB";



// api
var dev_token = '249788944:AAEp8gn5CM9T7WZF_m6QhQaW_XwTl928Rec';
var release_token = '231086512:AAEut2ULdDdJgvjZLLOxRi0i_lqJ-YUmTRE';
// var TOKEN = '263566152:AAFJYhlpEaurVQqOJhHBEURuUxWEeuuW3Q8';


var TOKEN = release_token;
var BOTAN_TOKEN = botanAPI_release;
var botan = require('botanio')(BOTAN_TOKEN);


var USER = 'USER_ID';

var bot = new TelegramBot(TOKEN,{polling:true});
console.log("bot is running");


// Matches /echo [whatever]
bot.onText(/\/help/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/help');
  //bot.sendMessage(fromId, "type anything to read cat sound");
  //bot.sendMessage(fromId, "/randomcat to see a random cat");
  bot.sendMessage(chatId ,TheCatAPI.helpMessage);
});

bot.onText(/\/start/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/help');
  //bot.sendMessage(fromId, "type anything to read cat sound");
  //bot.sendMessage(fromId, "/randomcat to see a random cat");
  bot.sendMessage(chatId ,TheCatAPI.helpMessage);
});

bot.onText(/\/about/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/about');
  bot.sendMessage(chatId, "MeowPicBot fetch a cat from TheCatAPI for you.\nBot created by @ThaiThien");
});


bot.onText(/\/meow/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/meow');
  TheCatAPI.sendCat(bot,msg,null);
});

bot.onText(/\/hats/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/hats');
  TheCatAPI.sendCat(bot,msg,"hats");
});

bot.onText(/\/sunglasses/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
   botan.track(msg, '/sunglasses');
  TheCatAPI.sendCat(bot,msg,"sunglasses");
});

bot.onText(/\/boxes/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/boxes');
  TheCatAPI.sendCat(bot,msg,"boxes");
});

// caturday
bot.onText(/\/caturday/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/caturday');
  TheCatAPI.sendCat(bot,msg,"caturday");
});

//ties
bot.onText(/\/ties/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/ties');
  TheCatAPI.sendCat(bot,msg,"ties");
});

//ties
bot.onText(/\/sinks/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/sinks');
  TheCatAPI.sendCat(bot,msg,"sinks");
});

bot.onText(/\/clothes/, function (msg, match) {
  var fromId = msg.from.id;
  var chatId = msg.chat.id;
  var resp = match[1];
  botan.track(msg, '/clothes');
  TheCatAPI.sendCat(bot,msg,"clothes");
});


// reply to any message
/*
bot.on('message',function(msg){
    var chatId = msg.chat.id;
    var bot_response = "Meow meow meow meow";
    console.log(msg);
    if (msg.text.indexOf("/")==-1){
      bot.sendMessage(chatId,bot_response);
    }
});
*/

// Keep heroku happy
http.createServer(function (request, response) {}).listen(process.env.PORT || 5469)
